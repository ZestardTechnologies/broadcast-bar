<div class="breakingNews" id="<?php echo $newsData->encrypt_id; ?>">
    <?php if ($newsData->show_title == 1): ?>
        <div class="bn-title" style="width: auto;">
            <h2 style="display: inline-block;"><?php echo $newsData->title; ?></h2>

            <span></span>

        </div>	

    <?php endif; ?>

    <?php
    $newsDetail['ticker'] = $newsDetails;

    foreach ($newsDetail as $key => $newsText) {

        $count = count($newsText['text']);

        //echo $count;
        //echo '<pre>'; print_r($newsText); die;

        echo '<ul>';

        for ($i = 0; $i < $count; $i++) {
            ?>		

            <li>

        <?php
        if ($newsText['link'][$i] == 1) {
            ?>

                    <p class="<?php echo ($newsData->moving_text == '1' ? 'marquee' : 'not-marquee') ?>">

                        <span style="color:black"><a href="<?php echo $newsText['url'][$i] ?>" target="<?php echo ($newsText['target'][$i] == '1' ? '_blank' : '_parent') ?>"><?php echo ($newsData->moving_text == '1' ? $newsText['text'][$i] : str_limit($newsText['text'][$i])); ?></a></span>

                    </p>

            <?php
        } else {
            ?>

                    <p class="<?php echo ($newsData->moving_text == '1' ? 'marquee' : 'not-marquee') ?>">

                        <span style="color:black"><?php echo ($newsData->moving_text == '1' ? $newsText['text'][$i] : str_limit($newsText['text'][$i])); ?></span>

                    </p>

        <?php } ?>

            </li>		

        <?php
    }

    echo '</ul>';
}

if ($newsData->slide_type == 0) {
    ?>

        <div class="bn-navi ">

            <span></span>

            <span></span>

        </div>

        <?php
    } else {
        ?>

        <div class="bn-navi vertical">

            <span></span>

            <span></span>

        </div>

    <?php } ?>

</div>



<script>

    jQuery(document).ready(function () {

        jQuery("#<?php echo $newsData->encrypt_id; ?>").breakingNews({

            effect: "<?php echo ($newsData->slide_type == '0' ? 'slide-h' : 'slide-v') ?>",

            autoplay:<?php echo $newsData->autoplay; ?>,

            timer:<?php echo $newsData->autoplay_timing; ?>,

            color: "<?php echo $newsData->bgcolor; ?>",

            border:<?php echo $newsData->want_border; ?>,

        });

    });

</script>



<style type="text/css">

    #<?php echo $newsData->encrypt_id; ?> .marquee span {

        animation-direction: <?php echo ($newsData->direction == 'initial' ? 'initial' : 'reverse') ?>;

        animation-duration: <?php echo ($newsData->speed) . 's' ?>;

    }

</style>

