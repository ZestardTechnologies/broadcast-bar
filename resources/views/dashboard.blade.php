@extends('header')
@section('content')
    <div class="newsticker-container dashboardpage">
      <div class="">  
		<div class="success-copied"></div>   	
        <div class="col-sm-12 col-md-3">
	      	<div class="panel panel-default dashboard-addnews">                
	      	    <a href ="{{ url('addnewsticker') }}"><i class="fa fa-plus fa-3x" aria-hidden="true"></i>Add New Bar</a>
            </div>
        </div>
        @foreach($data as $title)
            <div class="col-sm-12 col-md-3">
                <div class="dashboard-showtittle">
                    <i class="fa fa-cogs fa-4x" aria-hidden="true"></i>
        	        <a class="news-tittle" href="#">{{ str_limit($title->title,20,'...')}}</a>
                    <div class="col-md-12 sub-maindiv">                  
                        <div class="col-sm-12 col-md-4 sub-showdiv div-width"><a href="{{ url('editnewsticker/'.$title->encrypt_id) }}"><i class="fa fa-pencil" aria-hidden="true"></i>Edit</a></div>                        
                        <div class="col-sm-12 col-md-4 sub-showdiv div-width1">
                            <textarea style="display:none;" rows="3" class="form-control short-code ticker-shortcode"  readonly="">&lt;div class="zestard-newsticker" news-id="<?php echo $title->encrypt_id; ?>"&gt;&lt;/div&gt;</textarea>
                            <a href="#" type="button" class="copyMe" onclick="copyFromDashboard(this)"><i class="fa fa-code" aria-hidden="true"></i>Shortcode</a>
                        </div>
                        <div class="col-sm-12 col-md-4 sub-showdiv div-width"><a href="{{ url('deletenewsticker/'.$title->news_id) }}" onclick="return confirm('Are you sure you want to delete?')"><i class="fa fa-trash" aria-hidden="true"></i>Delete</a></div>
                    </div>
                </div>
            </div>
        @endforeach        
      </div>
    </div>
	<div class="modal fade" id="new_note">
		<div class="modal-dialog">          
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title"><b>Note</b></h4>
				</div>
				<div class="modal-body">
					<p>Dear Customer, As this is a paid app and hundreds of customers are using it, So if you face any issue(s) on your store, before uninstalling, Please contact support team (<a href="mailto:support@zestard.com">support@zestard.com</a>) or live chat at bottom right to resolve it ASAP.</p>
				</div>        
				<div class="modal-footer">			
					<div class="datepicker_validate" id="modal_div">
						<div>
							<strong>Show me this again</strong>
								<span class="onoff"><input name="modal_status" type="checkbox" checked id="dont_show_again"/>							
								<label for="dont_show_again"></label></span>
						</div>      
					</div>      
				</div>      
			</div>
		</div>
	</div>
<script type="text/javascript">
	$("#dont_show_again").change(function(){
		var checked   = $(this).prop("checked");
		var shop_name = "{{ session('shop') }}";			
		if(!checked)
		{
			$.ajax({
				url:'update-modal-status',
				data:{shop_name:shop_name},
				async:false,					
				success:function(result)
				{
					
				}
			});				
			$('#new_note').modal('toggle');
		}
	});
	if("{{ $new_install }}")
	{
		var new_install = "{{ $new_install }}";	
		if(new_install == "Y")
		{
			$('#new_note').modal('show');
		}
	}		
</script>
	
@endsection
