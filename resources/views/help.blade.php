@extends('header')
@section('content')
<script type="text/javascript">
    ShopifyApp.ready(function (e) {
        ShopifyApp.Bar.initialize({
            title: 'Help',
            buttons: {
            }
        });
    });
</script>
<?php $store_name = session('shop'); ?>
<link rel="stylesheet" href="{{ asset('css/design_style.css') }}" />
<div class="container formcolor formcolor_help" >
    <div class=" row">
        <div class="modal fade" id="imagemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
            <div class="modal-dialog">
                <div class="modal-content">              
                    <div class="modal-body">
                        <button type="button" class="close" data-dismiss="modal">
                            <span aria-hidden="true">×</span>
                            <span class="sr-only">Close</span>
                        </button>
                        <img src="" class="imagepreview" style="width: 100%;">
                    </div>
                </div>
            </div>
        </div>
        <div class="help_page">            
            <div class="col-md-12 col-sm-12 col-xs-12 need_help">
                <!--<a href="https://apps.shopify.com/broadcast-bar?reveal_new_review=true" target="_blank">review</a>-->
                <h2 class="dd-help">Need Help?</h2>
                <p class="col-md-12"><b>To customize any thing within the app or for other work just contact us on below details</b></p>
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <ul class="dd-help-ul">
                        <li><span>Developer: </span><a target="_blank" href="https://www.zestard.com">Zestard Technologies Pvt Ltd</a></li>
                        <li><span>Email: </span><a href="mailto:support@zestard.com">support@zestard.com</a></li>
                        <li><span>Website: </span><a target="_blank" href="https://www.zestard.com">https://www.zestard.com</a></li>
                    </ul>
                </div>
            </div>

            <div class="col-md-12 col-sm-12 col-xs-12">
                <h2 class="dd-help">Configuration Instruction</h2> 
                <div class="col-md-12 col-sm-12 col-xs-12 help_accordians">
                    <div class="panel-group" id="accordion">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <p data-toggle="collapse" data-parent="#accordion" href="#collapse1"> 
                                        <strong><span class="">How to add Broadcast Bar?</span>
                                            <span class="fa fa-chevron-down pull-right"></span></strong>  
                                    </p>
                                </h4>
                            </div>
                            <div id="collapse1" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <ul class="ul-help">
                                        <li>For adding new Broadcast Bar click on <b>"Add New Bar"</b>.</li>
                                        <li>Onclick it will redirect to add page, where merchant have to add the details according to their needs, how they want to give look to thier broadcast bar.</li>
                                    </ul>                                    
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <p data-toggle="collapse" data-parent="#accordion" href="#collapse2"> 
                                        <strong><span class="">Can Broadcast Bar be on auto-play setting?</span>
                                            <span class="fa fa-chevron-down pull-right"></span></strong>  
                                    </p>
                                </h4>
                            </div>
                            <div id="collapse2" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <p>Yes, In Broadcast Bar app you can set autoplay to yes.</p> 
                                    <p>If you have more text then you can set it to autoplay and also set the autoplay timing when to show next text.</p>
                                </div>
                            </div>
                        </div>

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <p data-toggle="collapse" data-parent="#accordion" href="#collapse3"> 
                                        <strong><span class="">Text in Broadcast Bar will be static or moving?</span>
                                            <span class="fa fa-chevron-down pull-right"></span></strong>  
                                    </p>
                                </h4>
                            </div>
                            <div id="collapse3" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <p>If merchant want to keep the text move, then configure the <b>"Moving Text Setting"</b> set yes to moving text. You can also change the direction and speed of text to move.</p>
                                </div>
                            </div>
                        </div>

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <p data-toggle="collapse" data-parent="#accordion" href="#collapse4"> 
                                        <strong><span class="">Can merchant can set redirect link to the text?</span>
                                            <span class="fa fa-chevron-down pull-right"></span></strong>  
                                    </p>
                                </h4>
                            </div>
                            <div id="collapse4" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <p>Yes, merchant can set link on text in broadcast bar. And can also set in which page the link should open in same or in new window. They just have to set url in particular URL field below text field.</p>
                                </div>
                            </div>
                        </div>

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <p data-toggle="collapse" data-parent="#accordion" href="#collapse5"> 
                                        <strong><span class="">Where to paste the shortcode?</span>
                                            <span class="fa fa-chevron-down pull-right"></span></strong>  
                                    </p>
                                </h4>
                            </div>
                            <div id="collapse5" class="panel-collapse collapse">
                                <div class="success-copied"></div>
                                <div class="panel-body">
                                    <ul class="ul-help">						
                                        <li>After saving the details, you can copy the <b>Shortcode</b> from the options.<a class="screenshot" href="javascript:void(0)" image-src="{{ asset('image/dashboad-shortcode-shopify.png') }}"><b> See Example</b></a></li>

                                        <li>If you want to show <b>Broadcast Bar</b> in any page, click on <a href="https://<?php echo $store_name; ?>/admin/pages/new" target="_blank"><b>Page</b></a> and paste above shortcode in that page.<a class="screenshot" href="javascript:void(0)" image-src="{{ asset('image/page-shortcode-shopify.png') }}"><b> See Example</b></a></li>

                                        <li><b>★★★ You can show same BROADCAST BAR in whole website or every page of your website by pasting the shortcode in header, just click on <a href="https://<?php echo $store_name; ?>/admin/themes/current/?key=sections/header.liquid" target="_blank"><b>Header.liquid</b></a> and paste copied shortcode in header.liquid file.<a class="screenshot" href="javascript:void(0)" image-src="{{ asset('image/headershortcode-bar.png') }}"><b> See Example</b></a> and if header.liquid file is not there then click on<a href="https://<?php echo $store_name; ?>/admin/themes/current/?key=layout/theme.liquid" target="_blank"><b>Theme.liquid</b></a> and paste copied shortcode in theme.liquid file.★★★<a class="screenshot" href="javascript:void(0)" image-src="{{ asset('image/themeshortcode-bar.png') }}"><b> See Example</b></a></li>
                                    </ul>                                
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>


            <!--Uninstall Process Div start-->
            <div class="col-md-12 col-sm-12 col-xs-12">
                <h2 class="dd-help">Uninstall Instruction</h2> 
                <div class="col-md-12 col-sm-12 col-xs-12 help_accordians">
                    <div class="panel-group" id="accordion">                        
                        <ul class="ul-help">
                            <li>To remove the App, go to <a href="https://<?php echo $store_name; ?>/admin/apps" target="_blank"><b>Apps</b></a>.</li>
                            <li>Click on delete icon of Broadcast Bar App.<a class="screenshot" href="javascript:void(0)" image-src="{{ asset('image/uninstall-app-shopify.png') }}"><b> See Example</b></a></li>
                            <li>If possible then remove shortcode where you have pasted.</li>
                        </ul>                        
                    </div>
                </div>
            </div>
            <!--Uninstall Process Div end-->
        </div>
        <!--Version updates-->
        <!--        <div class="version_update_section">
                    <div class="col-md-6" style="padding-right: 0;">
                        <div class="feature_box">
                            <h3 class="dd-help">Version Updates <span class="verison_no">2.0</span></h3>
                            <div class="version_block">
                                <div class="col-md-12">
                                    <div class="col-md-3 version_date">
                                        <p><i class="glyphicon glyphicon-heart"></i></p>
                                        <strong>22 Jan, 2018</strong>
                                        <a href="#"><b>Update</b></a>
                                    </div>
                                    <div class="col-md-8 version_details">
                                        <strong>Version 2.0</strong>
                                        <ul>
                                            <li>Add Delivery Date & Time information under customer order Email</li>
                                            <li>Auto Select for Next Available Delivery Date</li>
                                            <li>Auto Tag Delivery Details to all the Orders</li>
                                            <li>Manage Cut Off Time for Each Individual Weekday</li>
                                            <li>Limit Number of Order Delivery during the given time slot for any day </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="col-md-3 version_date">
                                        <p><i class="glyphicon glyphicon-globe"></i></p>
                                        <strong>20 Dec, 2017</strong>
                                        <a href="#"><b>Release</b></a>
                                    </div>
                                    <div class="col-md-8 version_details version_details_2">
                                        <strong>Version 1.0</strong>
                                        <ul>
                                            <li>Delivery Date & Time Selection</li>
                                            <li>Same Day & Next Day Delivery</li>
                                            <li>Blocking Specific Days and Dates</li>
                                            <li>Admin Order Manage & Export Based on Delivery Date</li>
                                            <li>Option for Cut Off Time & Delivery Time</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="feature_box">
                            <h3 class="dd-help">Upcoming Features</h3>
                            <div class="feature_block">
                                <div>   
                                    <span class="checkboxFive">
                                        <input type="checkbox" checked disabled value="1" id="checkbox0" name="exclude_block_date_status">
                                        <label for="checkbox0"></label>
                                    </span> 
                                    <strong>Multiple Cutoff Time Option</strong>  
                                </div>
                                <div>   
                                    <span class="checkboxFive">
                                        <input type="checkbox" checked disabled value="1" id="checkbox4" name="exclude_block_date_status">
                                        <label for="checkbox4"></label>
                                    </span> 
                                    <strong>Multiple Delivery Time Option</strong>  
                                </div>
                                <div>   
                                    <span class="checkboxFive">
                                        <input type="checkbox" checked disabled value="1" id="checkbox5" name="exclude_block_date_status">
                                        <label for="checkbox5"></label>
                                    </span> 
                                    <strong>Auto Tag Delivery Details to all the Orders Within Interval of 1 hour from Order Placed Time</strong>  
                                </div>
                                <div>   
                                    <span class="checkboxFive">
                                        <input type="checkbox" checked disabled value="1" id="checkbox1" name="exclude_block_date_status">
                                        <label for="checkbox1"></label>
                                    </span> 
                                    <strong>Auto Select for Next Available Delivery Date</strong>  
                                </div>
                                <div>   
                                    <span class="checkboxFive">
                                        <input type="checkbox" checked disabled value="1" id="checkbox2" name="exclude_block_date_status">
                                        <label for="checkbox2"></label>
                                    </span> 
                                    <strong>Order Export in Excel</strong>  
                                </div>
                                <div>   
                                    <span class="checkboxFive">
                                        <input type="checkbox" checked disabled value="1" id="checkbox3" name="exclude_block_date_status">
                                        <label for="checkbox3"></label>
                                    </span> 
                                    <strong>Filtering Orders by Delivery Date</strong>  
                                </div>                                        
                            </div>
                            <div>
                                <p class="feature_text">
                                    New features are always welcome send us on : <a href="mailto:support@zestard.com"><b>support@zestard.com</b></a> 
                                </p>
                            </div>
                        </div>
                    </div>
                </div>-->

    </div>
</div>
<script>
    $(document).ready(function () {
        // Add minus icon for collapse element which is open by default
        $(".collapse.in").each(function () {
            $(this).siblings(".panel-heading").find(".fa").addClass("fa-chevron-up").removeClass("fa-chevron-down");
        });
        // Toggle plus minus icon on show hide of collapse element
        $(".collapse").on('show.bs.collapse', function () {
            $(this).parent().find("span.fa").removeClass("fa-chevron-down").addClass("fa-chevron-up");
        }).on('hide.bs.collapse', function () {
            $(this).parent().find("span.fa").removeClass("fa-chevron-up").addClass("fa-chevron-down");
        });
    });
</script>
<script>
    var el1 = document.getElementById('copyproductBtn');
    var el2 = document.getElementById('copyblogBtn');
    var el3 = document.getElementById('copyrandomproductBtn');
    if (el1) {
        el1.addEventListener("click", function () {
            copyToClipboard(document.getElementById("product-shortcode"));
        });
    }
    if (el2) {
        el2.addEventListener("click", function () {
            copyToClipboard(document.getElementById("blog-shortcode"));
        });
    }
    if (el3) {
        el3.addEventListener("click", function () {
            copyToClipboard(document.getElementById("randomproduct-shortcode"));
        });
    }
    /*document.getElementById("copyproductBtn").addEventListener("click", function () {
     copyToClipboard(document.getElementById("product-shortcode"));
     });
     document.getElementById("copyblogBtn").addEventListener("click", function () {
     copyToClipboard(document.getElementById("blog-shortcode"));
     });
     document.getElementById("copyrandomproductBtn").addEventListener("click", function () {
     copyToClipboard(document.getElementById("randomproduct-shortcode"));
     });*/


    function copyToClipboard(elem) {
        var targetId = "_hiddenCopyText_";
        var isInput = elem.tagName === "INPUT" || elem.tagName === "TEXTAREA";
        var origSelectionStart, origSelectionEnd;
        if (isInput) {
            target = elem;
            origSelectionStart = elem.selectionStart;
            origSelectionEnd = elem.selectionEnd;
        } else {
            target = document.getElementById(targetId);
            if (!target) {
                var target = document.createElement("textarea");
                target.style.position = "absolute";
                target.style.left = "-9999px";
                target.style.top = "0";
                target.id = targetId;
                document.body.appendChild(target);
            }
            target.textContent = elem.textContent;
        }
        var currentFocus = document.activeElement;
        target.focus();
        target.setSelectionRange(0, target.value.length);
        var succeed;
        try {
            succeed = document.execCommand("copy");
        } catch (e) {
            succeed = false;
        }
        if (currentFocus && typeof currentFocus.focus === "function") {
            currentFocus.focus();
        }
        if (isInput) {
            elem.setSelectionRange(origSelectionStart, origSelectionEnd);
        } else {
            target.textContent = "";
        }
        return succeed;
    }
</script>
@endsection
