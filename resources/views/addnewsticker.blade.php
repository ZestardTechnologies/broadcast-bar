@extends('header')

@section('content')

<div class="formpage">

    <div class="newsticker-container">

      <div class="">

      	<div class="col-md-12 addnews">

      	<h2 class="sub-heading">Broadcast Bar Details</h2>

      		<form id="newsform" method="post" data-toggle="validator" action="{{ action('NewstickerController@SaveNewsticker') }}">

      		<input type="hidden" name="_token" value="{{ csrf_token() }}">
      			<div class="section">

      			<h2><b>General Setting:</b></h2>

				  	<div class="col-md-12 form-group">

					    <label for="title">Title</label>

					    <input type="text" class="form-control" id="title" name="title" required>

					    <div class="comment">

					    	<p><b>Note: </b>Please try to make your Title between 20 to 25 characters, So that News Ticker will look great.</p>

					    </div>			    

				  	</div>

				  	

				  	<div class="col-md-12">

				  		<div class="col-md-2 form-group box-space">

						    <label for="status">App Status?</label>

						    <select class="form-control" id="status" name="status">

						    	<option value="1">Enabled</option>

							    <option value="0">Disabled</option>						    				    

							</select>

					  	</div>



					  	<div class="col-md-2 form-group box-space">

						    <label for="show-title">Show Title</label>

						    <select class="form-control" id="show-title" name="show-title">

							    <option value="1">Yes</option>

							    <option value="0">No</option>				    

						 	</select>

					  	</div>				  	

					  

					  	<div class="col-md-2 form-group  box-space">

						    <label for="bgcolor">Background Color</label>
                                                    
						    <select class="form-control" id="bgcolor" name="bgcolor">

							    <option value="light">light</option>

							    <option value="green">green</option>

							    <option value="yellow">Yellow</option>

							    <option value="turquoise">Turquoise</option>

							    <option value="orange">Orange</option>

							    <option value="purple">Purple</option>

							    <option value="darkred">Dark Red</option>

							    <option value="black">Black</option>

							    <option value="pink">Pink</option>				    

							</select>

					  	</div>



					  	<div class="col-md-2 form-group box-space">

						    <label for="want-border">Set Border</label>

						    <select class="form-control" id="want-border" name="want-border">

							    <option value="0">No</option>

							    <option value="1">Yes</option>				    

						 	</select>

					  	</div>

					  	

					  	<div class="col-md-2 form-group">

					    	<label for="slide-type">Slide Type</label>

					    	<select class="form-control" id="slide-type" name="slide-type">

							    <option value="0">Horizontal</option>

							    <option value="1">Vertical</option>				    

						  	</select>

					  	</div>

					</div>

				</div>

				<hr>

				  	

			  	<div class="section">

			  	<h2><b>Auto-Play Setting:</b></h2>

			  	<div class="col-md-12">

				  	<div class="col-md-3 form-group box-space">

					    <label for="autoplay">Set Autoplay</label>

					    <select class="form-control" id="auto-play" name="autoplay">

						    <option value="0">No</option>

						    <option value="1">Yes</option>				    

						</select>

				  	</div>	



				  	<div class="col-md-3 form-group box-space">

					    <label for="autoplay-timing">Autoplay Timing</label>

					    <input type="number" class="form-control" id="autoplay-timing" name="autoplay-timing" value="12000">

					    <div class="comment">

					    	<p><b>Note: </b>Set auto play timing, For example- 10000 means 10 seconds.</p>

					    </div>

				  	</div>

				</div>

				</div>

				<hr>



				<div class="section">

				<h2><b>Moving Text Setting:</b></h2>

				<div class="col-md-12">

				  	<div class="col-md-3 form-group box-space">

					    <label for="moving-text">Moving Text</label>

					    <select class="form-control" id="moving-text" name="moving-text">

					    	<option value="0">No</option>

						    <option value="1">Yes</option>						    

						</select>

				  	</div>	  	



				  	<div class="col-md-3 form-group box-space">

					    <label for="direction">Direction</label>

					    <select class="form-control" id="direction" name="direction">

					    	<option value="initial">Right to Left</option>

						    <option value="reverse">Left to Right</option>						    				    

						</select>

				  	</div>



				  	<div class="col-md-3 form-group">

					    <label for="speed">Speed</label>

					    <input type="number" class="form-control" id="speed" name="speed" value="15">

					    <div class="comment">

					    	<p><b>Note: </b>Set Speed for moving text, For example- 15 means 15 seconds.</p>

					    </div>

				  	</div>

				</div>

				</div>

				<hr>



			   	<div class="col-md-12 news-details">

			   	<div class="col-md-11 news-details-innerdiv">

			   		<div class="col-md-12 form-group">

	        			<label for="data[text]">Text</label>

	        			<input type="text" class="form-control text" name="data[text][0]" id="data[text][0]" required/>

	        		</div>



	        		<div class="col-md-3 form-group box-space">

						<label for="data[link]">Set Link?</label>

					    <select class="form-control link" id="data[link][0]" name="data[link][0]">

						    <option value="0">No</option>

						    <option value="1">Yes</option>				    

						</select>

					</div>



					<div class="col-md-3 form-group box-space">

						<label for="data[target]">Target</label>

					    <select class="form-control target" id="data[target][0]" name="data[target][0]">

						    <option value="0">Same Window</option>

						    <option value="1">New Window</option>				    

						</select>

					</div>



					<div class="col-md-6 form-group">

						<label for="data[url]">URL</label>

	        			<input type="text" class="form-control url" name="data[url][0]" id="data[url][0]"/>

	        		</div>

    			</div>

    			</div>

    			<div id="add-details"><i class="fa fa-plus" aria-hidden="true"></i>More News</div>



			  <input type="submit" name="save" class="btn btn-primary" value="SAVE">

			  <a class="goback-right" href="{{ url('/dashboard') }}"><i class="fa fa-angle-double-left" aria-hidden="true"></i>GO BACK</a>

			</form>

      	</div>       

      </div>

    </div>

</div>



    <script type="text/javascript">

	jQuery(document).ready(function(){

		jQuery("#add-details").click(function(e){

	    e.preventDefault();

	        var numberOfText = jQuery("#newsform").find("input[name^='data[text]']").length;

	        //console.log(numberOfText);

	        var label1 = '<div class="col-md-11 news-details-innerdiv"><div class="col-md-12 form-group"><label for="data[text]">Text </label> ';

	        var input1 = '<input type="text" class="form-control text" name="data[text][' + numberOfText + ']" id="data[text][' + numberOfText + ']" required/></div>';



	        var label2 = '<div class="col-md-3 form-group box-space"><label for="data[link]">Set Link?</label>';

	        var input2 = '<select class="form-control link" name="data[link][' + numberOfText +']" id="data[link][' + numberOfText +']"><option value="0">No</option><option value="1">Yes</option></select></div>';



	        var label3 = '<div class="col-md-3 form-group box-space"><label for="data[target]">Target</label>';

	        var input3 = '<select class="form-control target" name="data[target][' + numberOfText +']" id="data[target][' + numberOfText +']"><option value="0">Same Window</option><option value="1">New Window</option></select></div>';



	        var label4 = '<div class="col-md-6 form-group"><label for="data[url]">URL</label> ';

	        var input4 = '<input type="text" class="form-control url" name="data[url][' + numberOfText + ']" id="data[url][' + numberOfText + ']" /></div></div>';



	        var removeButton = '<div class="col-md-1 form-group"><button class="remove-text"><i class="fa fa-times" aria-hidden="true"></i></button></div>';

	        var html = "<div class='col-md-12 news-details'>" + label1 + input1 + label2 + input2 + label3 + input3 + label4 + input4 + removeButton + "</div>";

	        jQuery("#newsform").find("#add-details").before(html);



	    });

	});



	jQuery(document).ready(function(){

		jQuery("#newsform").on("click", ".remove-text",function(e){

			e.preventDefault();

			jQuery(this).parents(".news-details").remove();



			var numItems = jQuery('.news-details').length;

 			console.log(numItems);

 			

 			var i = 0;

 			jQuery(".news-details").each(function(){



 				jQuery(this).find("input[class^='form-control text']").attr("name" , "data[text]["+i+"]");

    			jQuery(this).find("select[class^='form-control link']").attr("name" , "data[link]["+i+"]");

    			jQuery(this).find("select[class^='form-control target']").attr("name" , "data[target]["+i+"]");

    			jQuery(this).find("input[class^='form-control url']").attr("name" , "data[url]["+i+"]");	



    			i++;

 			});

		});

	});

	</script>

@endsection



