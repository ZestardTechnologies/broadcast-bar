@extends('header')
@section('content')
<?php
$store_name = session('shop');
?>
<div class="container">
    <div class="row">
        <div class="card">

            <div class="modal fade" id="imagemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                <div class="modal-dialog">
                    <div class="modal-content">              
                        <div class="modal-body">
                            <button type="button" class="close" data-dismiss="modal">
                                <span aria-hidden="true">×</span>
                                <span class="sr-only">Close</span>
                            </button>
                            <img src="" class="imagepreview" style="width: 100%;">
                        </div>
                    </div>
                </div>
            </div>

            <div class="card-content help">
                <h5>Need Help?</h5>
                <p><b>To customize any thing within the app or for other work just contact us on below details</b></p>
                <ul>
                    <li>Developer: <b><a target="_blank" href="https://www.zestardshop.com">Zestard Technologies Pvt Ltd</a></b></li>
                    <li>Email: <b><a href="mailto:support@zestard.com">support@zestard.com</a></b></li>
                    <li>Website: <b><a target="_blank" href="https://www.zestardshop.com">https://www.zestardshop.com</a></b></li>
                </ul>
                <hr>

                <h5>General Instruction</h5>
                <h6><b>App Configuration:</b></h6>
                <ul class="limit">
                    <li>After Installing the App, It will show dashboard page, where store owner can add Broadcast Bar.</li>
                    <li>On dashboard, Broadcast Bar Title will appear (after adding "New Bar") with 3 option below: Edit, Shortcode and Delete.</li>
                    <li>In Edit you can edit all fields and can also see the preview of Broadcast Bar you have created.</li>
                    <li>On click shortcode will copy, and you can paste anywhere in your store ( <b>follow below steps</b> ).</li>
                    <li>On click delete Broadcast Bar will get deleted.</li>
                </ul>
                <h6><b>Uninstall App:</b></h6>
                <ul class="limit">
                    <li>To remove the App, go to <a href="https://<?php echo $store_name; ?>/admin/apps" target="_blank"><b>Apps</b></a>.</li>
                    <li>Click on delete icon of Broadcast Bar App.<a class="screenshot" href="javascript:void(0)" image-src="{{ asset('image/uninstall-app-shopify.png') }}"><b> See Example</b></a></li>
                    <li>If possible then remove shortcode where you have pasted.</li>
                </ul>
                <hr>

                <h5>Follow the Steps:</h5>
                <h6><b>Where to paste shortcode?</b></h6>
                <ul class="limit">						
                    <li>After saving the details, you can copy the <b>Shortcode</b> from the options.<a class="screenshot" href="javascript:void(0)" image-src="{{ asset('image/dashboad-shortcode-shopify.png') }}"><b> See Example</b></a></li>

                    <li>If you want to show <b>Broadcast Bar</b> in any page, click on <a href="https://<?php echo $store_name; ?>/admin/pages/new" target="_blank"><b>Page</b></a> and paste above shortcode in that page.<a class="screenshot" href="javascript:void(0)" image-src="{{ asset('image/page-shortcode-shopify.png') }}"><b> See Example</b></a></li>

                    <li><b>★★★ You can show same BROADCAST BAR in whole website or every page of your website by pasting the shortcode in header, just click on <a href="https://<?php echo $store_name; ?>/admin/themes/current/?key=sections/header.liquid" target="_blank"><b>Header.liquid</b></a> and paste copied shortcode in header.liquid file.<a class="screenshot" href="javascript:void(0)" image-src="{{ asset('image/headershortcode-bar.png') }}"><b> See Example</b></a> and if header.liquid file is not there then click on<a href="https://<?php echo $store_name; ?>/admin/themes/current/?key=layout/theme.liquid" target="_blank"><b>Theme.liquid</b></a> and paste copied shortcode in theme.liquid file.★★★<a class="screenshot" href="javascript:void(0)" image-src="{{ asset('image/themeshortcode-bar.png') }}"><b> See Example</b></a></li>
                </ul>

                <a class="goback" href="{{ url('dashboard') }}">
                    <img src="{!! asset('image/back.png') !!}">Go Back
                </a>
            </div>				
        </div>
    </div>
</div>
@endsection
<style> 
    .help h5{
        font-size: 24px;
    }
    .help h6{
        font-size: 16px;
    }
    .help a{
        color: #039be5;
    }
    .help ul{
        padding-left: 0px;
    }
    .limit {
        margin-left: 20px;
    }
    ul.limit li {
        list-style-type: disc !important;
        display: list-item;
    }
</style>