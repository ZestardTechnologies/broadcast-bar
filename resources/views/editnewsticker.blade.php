@extends('header')

@section('content')
<div class="formpage">
    <div class="newsticker-container">
	    <div class="">
	    <div class="col-md-6 editnews-wrapper"> 
      		<div class="editnews left-news">
      		<h2 class="sub-heading">Broadcast Bar Details</h2>
      			<form id="newsform" method="get" action="{{ url('updatenewsticker/'.$data->news_id) }}">
		      		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		      		<input type="hidden" name="id" value="{{ $data->news_id }}">

		      		<div class="modal fade" id="imagemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
						<div class="modal-dialog">
							<div class="modal-content">              
								<div class="modal-body">
									<button type="button" class="close" data-dismiss="modal">
										<span aria-hidden="true">×</span>
										<span class="sr-only">Close</span>
									</button>
									<img src="" class="imagepreview" style="width: 100%;">
								</div>
							</div>
						</div>
					</div>

				<div class="section">
      			<h2><b>General Setting:</b></h2>
					<div class="col-md-12 form-group">
					    <label for="title">Title</label>
					    <input type="text" class="form-control" id="title" name="title" value="{{ $data->title }}" required>
					    <div class="comment">
					    	<p><b>Note: </b>Please try to make your Title between 20 to 25 characters, So that News Ticker will look great.</p>
					    </div>
				  	</div>
				  
				  	<div class="col-md-4 form-group box-space">
				    	<label for="status">App Status?</label>
				    	<select class="form-control" id="status" name="status">
					    	<option value="0" <?php echo ($data->status == '0' ? ' selected' : '') ?>>Disabled</option>
					    	<option value="1" <?php echo ($data->status == '1' ? ' selected' : '') ?>>Enabled</option>				    
						</select>
				  	</div>

				  	<div class="col-md-4 form-group box-space">
					    <label for="show-title">Show Title</label>
					    <select class="form-control" id="show-title" name="show-title">
						    <option value="1" <?php echo ($data->show_title == '1' ? ' selected' : '') ?>>Yes</option>
						    <option value="0" <?php echo ($data->show_title == '0' ? ' selected' : '') ?>>No</option>				    
					 	</select>
				  	</div>

				  	<div class="col-md-4 form-group">
					    <label for="slide-type">Slide Type</label>
					    <select class="form-control" id="slide-type" name="slide_type">			    	
					    	<option value="0" <?php echo ($data->slide_type == '0' ? ' selected' : '') ?>>Horizontal</option>
					    	<option value="1" <?php echo ($data->slide_type == '1' ? ' selected' : '') ?>>Vertical</option>
						</select>
				  	</div>
				  	
				  	<div class="col-md-4 form-group box-space">
					    <label for="bgcolor">Background Color</label>
					    <select class="form-control" id="bgcolor" name="bgcolor">
						    <option value="light" <?php echo ($data->bgcolor == 'light' ? ' selected' : '') ?>>light</option>
						    <option value="green" <?php echo ($data->bgcolor == 'green' ? ' selected' : '') ?>>green</option>
						    <option value="yellow" <?php echo ($data->bgcolor == 'yellow' ? ' selected' : '') ?>>Yellow</option>
						    <option value="turquoise" <?php echo ($data->bgcolor == 'turquoise' ? ' selected' : '') ?>>Turquoise</option>
						    <option value="orange" <?php echo ($data->bgcolor == 'orange' ? ' selected' : '') ?>>Orange</option>
						    <option value="purple" <?php echo ($data->bgcolor == 'purple' ? ' selected' : '') ?>>Purple</option>
						    <option value="darkred" <?php echo ($data->bgcolor == 'darkred' ? ' selected' : '') ?>>Dark Red</option>
						    <option value="black" <?php echo ($data->bgcolor == 'black' ? ' selected' : '') ?>>Black</option>
						    <option value="pink" <?php echo ($data->bgcolor == 'pink' ? ' selected' : '') ?>>Pink</option>				    
						</select>
					</div>
				  
				  	<div class="col-md-4 form-group box-space">
					    <label for="want-border">Set Border</label>
					    <select class="form-control" id="want-border" name="want_border">
						    <option value="0" <?php echo ($data->want_border == '0' ? ' selected' : '') ?>>No</option>
						    <option value="1" <?php echo ($data->want_border == '1' ? ' selected' : '') ?>>Yes</option>				    
						</select>
				  	</div>
				</div>
				<hr>
				  	
				<div class="section">
      			<h2><b>Auto-Play Setting:</b></h2>
				  	<div class="col-md-4 form-group box-space">
				    	<label for="autoplay">Set Autoplay</label>
				    	<select class="form-control" id="auto-play" name="autoplay">
					    	<option value="0" <?php echo ($data->autoplay == '0' ? ' selected' : '') ?>>No</option>
					    	<option value="1" <?php echo ($data->autoplay == '1' ? ' selected' : '') ?>>Yes</option>				    
					  	</select>
				  	</div>

				  	<div class="col-md-4 form-group box-space">
					    <label for="autoplay-timing">Autoplay Timing</label>
					    <input type="number" class="form-control" id="autoplay-timing" name="autoplay-timing" value="{{ $data->autoplay_timing }}">
					    <div class="comment">
					    	<p><b>Note: </b>Set auto play timing, For example- 10000 means 10 seconds.</p>
					    </div>
				  	</div>
				</div>
				<hr>
				
				<div class="section">
      			<h2><b>Moving Text Setting:</b></h2> 	
				  	<div class="col-md-4 form-group box-space">
					    <label for="moving-text">Moving Text</label>
					    <select class="form-control" id="moving-text" name="moving-text">						    
					    	<option value="0" <?php echo ($data->moving_text == '0' ? ' selected' : '') ?>>No</option>
						    <option value="1" <?php echo ($data->moving_text == '1' ? ' selected' : '') ?>>Yes</option>					    
						</select>
				  	</div>

				  	<div class="col-md-4 form-group box-space">
					    <label for="direction">Direction</label>
					    <select class="form-control" id="direction" name="direction">
					    	<option value="initial" <?php echo ($data->direction == 'initial' ? ' selected' : '') ?>>Right to left</option>
						    <option value="reverse" <?php echo ($data->direction == 'reverse' ? ' selected' : '') ?>>Left to Right</option>						    				    
						</select>
				  	</div>

				  	<div class="col-md-4 form-group">
					    <label for="speed">Speed</label>
					    <input type="number" class="form-control" id="speed" name="speed" value="{{ $data->speed }}">
					    <div class="comment">
					    	<p><b>Note: </b>Set Speed for moving text, For example- 15 means 15 seconds.</p>
					    </div>
				  	</div>
				</div>
				<hr>

				  	<?php 
				  		if($newsdata == ""){
				  	?>
				  		<div class="col-md-12 news-details">
				  		<div class="col-md-11 news-details-innerdiv">
				  			<div class="col-md-12 form-group">
	        					<label for="data[text]">Text</label>
	        					<input type="text" class="form-control text" name="data[text][0]" id="data[text][0]" required/>
	        				</div>

	        				<div class="col-md-3 form-group box-space">
		        				<label for="data[link]">Set Link?</label>
							    <select class="form-control link" id="data[link][0]" name="data[link][0]">
								    <option value="0">No</option>
								    <option value="1">Yes</option>				    
								</select>
							</div>

							<div class="col-md-4 form-group box-space">
								<label for="data[target]">Target</label>
							    <select class="form-control target" id="data[target][0]" name="data[target][0]">
								    <option value="0">Same Window</option>
								    <option value="1">New Window</option>				    
								</select>
							</div>

							<div class="col-md-5 form-group">
								<label for="data[url]">URL</label>
			        			<input type="text" class="form-control url" name="data[url][0]" id="data[url][0]" />
			        		</div>
	    				</div>
	    				</div>

				  	<?php	
				  		}else{
				  			$arr['ticker'] =$newsdata;
				  			//echo '<pre>';print_r($arr);
				  			foreach($arr as $key => $value){
				  				//echo '<pre>';print_r($arr);
				  				$lastKey = max(array_keys($value['text']));//key(array_slice($value['text'], -1, 1, TRUE));
				  				//echo '<pre>';print_r($lastKey);
				  				for ($i=0; $i <= $lastKey; $i++) {
			  					if(isset($value['text'][$i])) {			  						
				  			?>
							  	<div class="col-md-12 news-details">
							  	<div class="col-md-11 news-details-innerdiv">
							  		<div class="col-md-12 form-group">
					        			<label for="data[text]">Text</label>
					        			<input type="text" class="form-control text" name="data[text][<?php echo $i;?>]" id="data[text][<?php echo $i; ?>]" value="<?php echo $value['text'][$i]; ?>" required/>
					        		</div>
				        			
				        			<div class="col-md-3 form-group box-space">
					        			<label for="data[link]">Set Link?</label>
									    <select class="form-control link" id="data[link][<?php echo $i;?>]" name="data[link][<?php echo $i; ?>]">
										    <option value="0" <?php echo ($value['link'][$i] == '0' ? 'selected' : '') ?>>No</option>
										    <option value="1" <?php echo ($value['link'][$i] == '1' ? 'selected' : '') ?>>Yes</option>				    
										</select>
									</div>

									<div class="col-md-4 form-group box-space">
										<label for="data[target]">Target</label>
									    <select class="form-control target" id="data[target][<?php echo $i;?>]" name="data[target][<?php echo $i; ?>]">
										    <option value="0" <?php echo ($value['target'][$i] == '0' ? 'selected' : '') ?>>Same Window</option>
										    <option value="1" <?php echo ($value['target'][$i] == '1' ? 'selected' : '') ?>>New Window</option>				    
										</select>
									</div>

									<div class="col-md-5 form-group">
										<label for="data[url]">URL</label>
					        			<input type="text" class="form-control url" name="data[url][<?php echo $i;?>]" id="data[url][<?php echo $i; ?>]" value="<?php echo $value['url'][$i]; ?>"/>
					        		</div>
					        	</div>

					        	<div class="col-md-1 form-group">
									<button class="remove-text"><i class="fa fa-times" aria-hidden="true"></i></button>
								</div>
				    			</div>
			    				<?php
			    					}
			    				}
	    					}	    					
	    				}
	    			?>
	    			<div id="add-details"><i class="fa fa-plus" aria-hidden="true"></i>More News</div>
					
					<input type="submit" name="save" class="btn btn-primary" value="SAVE">
					<input type="submit" name="continue" class="btn btn-primary" value="SAVE & CONTINUE">
					<a class="btn btn-danger" href="{{ url('deletenewsticker/'.$data->news_id) }}" onclick="return confirm('Are you sure you want to delete?')">DELETE</a>
					<a class="goback-right" href="{{ url('/dashboard') }}"><i class="fa fa-angle-double-left" aria-hidden="true"></i>GO BACK</a>
				</form>
      		</div>
      		</div>
      		<div class="col-md-6 editnews-wrapper"> 
      		<div class="editnews right-news">
      			<div class="news-shortcode">
					<h2 class="sub-heading">Shortcode</h2>
					<div class="success-copied"></div>
					<div class="view-shortcode">    					   					
						<textarea id="ticker-shortcode" rows="3" class="form-control short-code"  readonly="">&lt;div class="zestard-newsticker" news-id="<?php echo $data->encrypt_id; ?>"&gt;&lt;/div&gt;</textarea>
						<button type="button" onclick="copyToClipboard('#ticker-shortcode')" class="btn tooltipped tooltipped-s copyMe"
						style="display: block;"><i class="fa fa-check"></i>Copy to clipboard</button> 						
    				</div>    				
	    		</div>	    		
      		</div>
      		<div class="editnews right-news">
      			<h2 class="sub-heading">Preview</h2>
      				<?php
      					if($data->status == 1){
      				?>

	    			<div class="breakingNews" id="<?php echo $data->encrypt_id;?>">
	    			<?php if($data->show_title == 1): ?>
						<div class="bn-title" style="width: auto;">
							<h2 style="display: inline-block;"><?php echo $data->title;?></h2>
							<span></span>
						</div>
					<?php endif; ?>
						<?php

						$arr['ticker'] =$newsdata;
				  			foreach($arr as $key => $value){
				  				$lastKey = max(array_keys($value['text']));
				  				echo '<ul>';
				  				for ($i=0; $i <= $lastKey; $i++) {
			  					if(isset($value['text'][$i])) {		
						?>		
						<li>
							<?php 
								if($value['link'][$i] == 1){
							?>
							<p class="<?php echo ($data->moving_text == '1' ? 'marquee' : 'not-marquee') ?>">
								<span style="color:black"><a href="<?php echo $value['url'][$i] ?>" target="<?php echo ($value['target'][$i] == '1' ? '_blank' : '_parent')?>"><?php echo ($data->moving_text == '1' ? $value['text'][$i] : str_limit($value['text'][$i]));?></a></span>
							</p>
							<?php
								}else{
							?>
							<p class="<?php echo ($data->moving_text == '1' ? 'marquee' : 'not-marquee') ?>">
								<span style="color:black"><?php echo ($data->moving_text == '1' ? $value['text'][$i] : str_limit($value['text'][$i])); ?></span>
							</p>
							<?php } ?>
						</li>		
						<?php
								}}
							echo '</ul>';
							}
							if($data->slide_type == 0){
						?>
						<div class="bn-navi ">
								<span></span>
								<span></span>
						</div>
						<?php 
							}else{
						?>
						<div class="bn-navi vertical">
								<span></span>
								<span></span>
						</div>
						<?php } ?>
					</div>
					<?php } ?>
		   		</div>

		   		<div class="editnews right-news">
		   			<h2 class="sub-heading">Where to paste Shortcode?</h2>
		   				<?php
		   					$shop_id = $data->store_id;
		   					$storedata = DB::table('usersettings')->select('store_name')->where('id',$shop_id)->first();
		   					$store = (array)$storedata;
		   					$store_name = $store['store_name'];
		   				?>

		   				<ul class="dashboard-note">						
							<li>After saving the details, you can copy the <b>Shortcode</b> from top of left side.<a class="screenshot" href="javascript:void(0)" image-src="{{ asset('image/shortcode-shopify.png') }}"><b> See Example</b></a></li>

							<li>If you want to show <b>Broadcast Bar</b> in any page, click on <a href="https://<?php echo $store_name;?>/admin/pages/new" target="_blank"><b>Page</b></a> and paste above shortcode in that page.<a class="screenshot" href="javascript:void(0)" image-src="{{ asset('image/page-shortcode-shopify.png') }}"><b> See Example</b></a></li>

							<li><b>★★★ You can show same BROADCAST BAR in whole website or every page of your website by pasting the shortcode in header, just click on <a href="https://<?php echo $store_name;?>/admin/themes/current/?key=sections/header.liquid" target="_blank"><b>Header.liquid</b></a> and paste copied shortcode in header.liquid file.<a class="screenshot" href="javascript:void(0)" image-src="{{ asset('image/headershortcode-bar.png') }}"><b> See Example</b></a> and if header.liquid file is not there then click on <a href="https://<?php echo $store_name;?>/admin/themes/current/?key=layout/theme.liquid" target="_blank"><b>Theme.liquid</b></a> and paste copied shortcode in theme.liquid file.★★★<a class="screenshot" href="javascript:void(0)" image-src="{{ asset('image/themeshortcode-bar.png') }}"><b> See Example</b></li>
						</ul>
		   		</div>				  
	    	</div>   	    	
    	</div>
	</div>
</div>

<script>
	jQuery(document).ready(function(){
	    jQuery("#myBtn").click(function(){
	        jQuery("#myModal").modal();
	    });
	});
</script>

<script type="text/javascript">

	jQuery(document).ready(function(){
	    jQuery("#add-details").click(function(e){
	        e.preventDefault();
	        var numberOfText = jQuery("#newsform").find("input[name^='data[text]']").length;
	        //console.log(numberOfText);
	        var label1 = '<div class="col-md-11 news-details-innerdiv"><div class="col-md-12 form-group"><label for="data[text]">Text</label> ';
	        var input1 = '<input type="text" class="form-control text" name="data[text][' + numberOfText + ']" id="data[text][' + numberOfText + ']" required/></div>';

	        var label2 = '<div class="col-md-3 form-group box-space"><label for="data[link]">Set Link?</label>';
	        var input2 = '<select class="form-control link" name="data[link][' + numberOfText +']" id="data[link][' + numberOfText +']"><option value="0">No</option><option value="1">Yes</option></select></div>';

	        var label3 = '<div class="col-md-4 form-group box-space"><label for="data[target]">Target</label>';
	        var input3 = '<select class="form-control target" name="data[target][' + numberOfText +']" id="data[target][' + numberOfText +']"><option value="0">Same Window</option><option value="1">New Window</option></select></div>';

	        var label4 = '<div class="col-md-5 form-group"><label for="data[url]">URL</label> ';
	        var input4 = '<input type="text" class="form-control url" name="data[url][' + numberOfText + ']" id="data[url][' + numberOfText + ']" /></div></div>';

	        var removeButton = '<div class="col-md-1 form-group"><button class="remove-text"><i class="fa fa-times" aria-hidden="true"></i></button></div>';
	        var html = "<div class='col-md-12 news-details'>" + label1 + input1 + label2 + input2 + label3 + input3 + label4 + input4 + removeButton + "</div>";
	        jQuery("#newsform").find("#add-details").before(html);
	    });
	});

	jQuery(document).ready(function(){
		jQuery("#newsform").on("click", ".remove-text",function(e){
			e.preventDefault();
			jQuery(this).parents(".news-details").remove();

			var numItems = jQuery('.news-details').length;
 			console.log(numItems);
 			
 			var i = 0;
 			jQuery(".news-details").each(function(){

 				jQuery(this).find("input[class^='form-control text']").attr("name" , "data[text]["+i+"]");
    			jQuery(this).find("select[class^='form-control link']").attr("name" , "data[link]["+i+"]");
    			jQuery(this).find("select[class^='form-control target']").attr("name" , "data[target]["+i+"]");
    			jQuery(this).find("input[class^='form-control url']").attr("name" , "data[url]["+i+"]");	

    			i++;
 			});
		});
	});
</script>

<script type="text/javascript">
	jQuery(document).ready(function(){
		jQuery("#<?php echo $data->encrypt_id;?>").breakingNews({
			effect		:"<?php echo ($data->slide_type == '0' ? 'slide-h' : 'slide-v') ?>",
			autoplay	:<?php echo $data->autoplay;?>,
			timer		:<?php echo $data->autoplay_timing;?>,
			color		:"<?php echo $data->bgcolor; ?>",
			border 		:<?php echo $data->want_border;?>,
		});		
	});
</script>

<style type="text/css">
.marquee span {
    animation-direction: <?php echo ($data->direction == 'initial' ? 'initial' : 'reverse') ?>;
    animation-duration: <?php echo ($data->speed).'s' ?>;
}
.breakingNews>ul {
	left: <?php echo ($data->show_title == '0' ? '0' : '210px') ?>;
}
</style>
@endsection
