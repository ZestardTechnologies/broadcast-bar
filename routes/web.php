<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('callback', function () {
    dd('test');
});*/

Route::get('dashboard', 'callbackController@dashboard')->name('dashboard');

Route::get('callback', 'callbackController@index')->name('callback');

Route::get('redirect', 'callbackController@redirect')->name('redirect');

Route::any('change_currency', 'callbackController@Currency')->name('change_currency');

Route::post('search', function () {
    return view('search');
})->middleware('cors')->name('search');

Route::get('uninstall', 'callbackController@uninstall')->name('uninstall');

Route::get('payment_process', 'callbackController@payment_method')->name('payment_process');

Route::get('payment_success', 'callbackController@payment_compelete')->name('payment_success');

Route::get('help', function () {
    return view('help');
})->name('help');

Route::get('addnewsticker', 'NewstickerController@addnewsticker')->name('addnewsticker');

Route::post('save', 'NewstickerController@SaveNewsticker')->name('save');

Route::get('editnewsticker/{encrypt_id}', 'NewstickerController@EditNewsticker')->name('editnewsticker');

Route::get('updatenewsticker/{news_id}', 'NewstickerController@UpdateNewsticker')->name('updatenewsticker');

Route::get('deletenewsticker/{news_id}', 'NewstickerController@DeleteNewsticker')->name('deletenewsticker');

Route::get('frontend', 'FrontendController@frontend')->middleware('cors')->name('frontend');

Route::any('update-modal-status', 'callbackController@update_modal_status')->name('update-modal-status'); 





