<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AddNewsForm extends Model
{
    protected $table = 'addnewsticker';
	protected $primaryKey = 'news_id';
    protected $fillable = [
    						'title',
    						'show_title',
    						'slide_type',
    						'bgcolor',
    						'want_border',
    						'autoplay',
                            'autoplay_timing',
    						'status',
                            'moving_text',
    						'direction',
    						'speed',
    						'store_id',
    						'encrypt_id',
    						'news_details'
    					];
    public $timestamps = false;
}
