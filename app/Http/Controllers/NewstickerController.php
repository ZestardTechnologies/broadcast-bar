<?php

namespace App\Http\Controllers;

//use Request;
use Illuminate\Http\Request;
use App\AddNewsForm;
use App;
use DB;

class NewstickerController extends Controller
{
	public function addnewsticker()
    {
        return view('addnewsticker');
    } 

   public function SaveNewsticker(Request $request) {   		
        
        //$shop = $request->session()->get('shop');
        $shop = session('shop');
                
        $id = DB::table('usersettings')->where('store_name',$shop)->value('id');
        //dd($id);
    	$newsform = new AddNewsForm;
        $newsform->title = $request['title'];
        $newsform->show_title = $request['show-title']; 
        $newsform->slide_type = $request['slide-type'];
        $newsform->bgcolor = $request['bgcolor'];
        $newsform->want_border = $request['want-border'];
        $newsform->autoplay = $request['autoplay'];
        $newsform->autoplay_timing = $request['autoplay-timing'];
        $newsform->status = $request['status'];
        $newsform->moving_text = $request['moving-text'];
        $newsform->direction = $request['direction'];
        $newsform->speed = $request['speed'];        
        $newsform->store_id = $request['store_id'];
        
        $newstext = $request['data'];

		$sertext = base64_encode(serialize($newstext));
        // $unserialize = unserialize($sertext);
        // echo '<pre>';print_r ($unserialize);die('died');
        $query = DB::table('addnewsticker')->insertGetId(['title'=>$request['title'], 'show_title'=>$request['show-title'], 'slide_type'=>$request['slide-type'], 'bgcolor'=>$request['bgcolor'], 'want_border'=>$request['want-border'], 'autoplay'=>$request['autoplay'], 'autoplay_timing'=>$request['autoplay-timing'], 'status'=>$request['status'], 'moving_text'=>$request['moving-text'], 'direction'=>$request['direction'], 'speed'=>$request['speed'], 'store_id'=>$id, 'encrypt_id'=>"",'news_details'=>$sertext]);
        $insertedid = $query;
		DB::table('usersettings')->where('store_name',$shop)->update(['new_install' => 'N']);
        $news_encrypt = crypt($insertedid,"ze");
        $finaly_encrypt = str_replace(['/','.'], "Z", $news_encrypt);

        $update_encrypt = AddNewsForm::where('news_id',$insertedid)->update(['encrypt_id' => $finaly_encrypt]);

        $notification = array(
        'message' => 'Added Successfully.',
        'alert-type' => 'success'
        );
        return redirect()->route('dashboard')->with('notification',$notification);
    } 

    public function EditNewsticker($encrypt_id){
    	//echo $encrypt_id; die;
        $editdata = AddNewsForm::where('encrypt_id',$encrypt_id)->first();
     	$unserdata = unserialize(base64_decode($editdata->news_details));
    	//echo '<pre>'; print_r($unserdata); die;
        return view('editnewsticker', ['data' => $editdata, 'newsdata' => $unserdata]);
    }


    public function UpdateNewsticker(Request $request, $news_id){
        $updatedata = AddNewsForm::find($news_id);
        $input = $request->all();
        $newsdata = $request['data'];

        $this->validate($request, [
            'title' => 'required',
        ]);

        $updateser = base64_encode(serialize($newsdata));
      
        $inputarray = ['title' => $request['title'], 'show_title' => $request['show-title'], 'slide_type' => $request['slide_type'], 'bgcolor' => $request['bgcolor'], 'want_border' => $request['want_border'], 'autoplay' => $request['autoplay'], 'autoplay_timing' => $request['autoplay-timing'], 'status' => $request['status'], 'moving_text' => $request['moving-text'], 'direction' => $request['direction'], 'speed' => $request['speed'], 'news_details' => $updateser];        

        //echo '<pre>'; print_r($inputarray); die;
        
        $updatedata->update($inputarray);

        //echo '<pre>'; print_r($inputarray); die;

        $notification = array(
        'message' => 'Updated Successfully.',
        'alert-type' => 'success'
        );

        if(array_key_exists('continue',$input)){
            return redirect()->back()->with('notification',$notification);
            // return view('editnewsticker', array(
            //     'data' => $updatedata));
        } else {
            return redirect()->route('dashboard')->with('notification',$notification);
        }
    }

     public function DeleteNewsticker(Request $request, $news_id){     																									
        $deletedata = AddNewsForm::find($news_id);      
        $input = $request->all();
        $deletedata->delete($input);

        $notification = array(
        'message' => 'Deleted Successfully.',
        'alert-type' => 'success'
        );
        return redirect()->route('dashboard')->with('notification',$notification);   
    }
}
