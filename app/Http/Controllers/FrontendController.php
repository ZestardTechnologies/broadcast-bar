<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\AddNewsForm;
use App;
use DB;

class FrontendController extends Controller
{
    public function frontend(request $request){	
    	$dataId = $request->news_id;
    	$shop = $request->domain_name;

    	$id = DB::table('usersettings')->where('store_name',$shop)->value('id');
        //echo '<pre>'; print_r($id); die;      
    	$newsFormData = AddNewsForm::select()->where(['encrypt_id'=>$dataId,'store_id'=>$id, 'status'=>1])->first();
        //echo '<pre>'; print_r($newsFormData); die;      
        if($newsFormData == ""){
            return ("");
        }else{
    	   $unserializeNewsDetails = unserialize(base64_decode($newsFormData->news_details));
           //echo '<pre>'; print_r($unserializeNewsDetails); die;
           if($shop == "morninde.myshopify.com"){
                return view('morninde_frontend', ['newsData' => $newsFormData, 'newsDetails' => $unserializeNewsDetails]);
           }else{
                return view('frontend', ['newsData' => $newsFormData, 'newsDetails' => $unserializeNewsDetails]);
           }
           
        }    	    	
    }
}
