 var base_path_broadcast_bar = "https://zestardshop.com/shopifyapp/broadcast-bar/public/";
 jQuery(document).ready(function() {
     var css = document.createElement("link");

     css.type = "text/css";

     css.rel = "stylesheet";

     css.href = "//zestardshop.com/shopifyapp/broadcast-bar/public/css/newsticker.css";

     document.getElementsByTagName("head")[0].appendChild(css);

     var scripts = [base_path_broadcast_bar + "js/newsticker.js"];

     for (index = 0; index < scripts.length; ++index) {

         var script = document.createElement('script');

         script.src = scripts[index];

         script.type = 'text/javascript';

         var done = false;

         script.onload = script.onreadystatechange = function() {

             if (!done && (!this.readyState || this.readyState == "loaded" || this.readyState == "complete")) {

                 done = true;

                 //promptForUserEntries();

             }

         };

         document.getElementsByTagName("head")[0].appendChild(script);

     }

     jQuery(".zestard-newsticker").each(function() {

         var news_id = jQuery(this).attr("news-id");

         var domain_name = Shopify.shop;

         var element = this;

         jQuery.ajax({

             type: "GET",

             url: base_path_broadcast_bar + "frontend",

             data: { news_id: news_id, domain_name: domain_name },

             success: function(response)

             {

                 jQuery(element).html(response);

             }

         });

     });

 });